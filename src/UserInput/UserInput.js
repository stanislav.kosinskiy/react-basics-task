import React from 'react'
import './UserInput.css'

const userInput = (props) => {

    const style = {
        fontWeight: "bold",
        textAlign: "center"
    }

    return (
        <div className="Input">
            <input style={style} onChange={props.changed} type="text" value={props.username} />
        </div>
        
    );
}

export default userInput;