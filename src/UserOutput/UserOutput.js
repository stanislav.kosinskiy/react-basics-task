import React from 'react'
import './UserOutput.css'

const userOutput = (props) => {
    return (
        <div className="Output">
            <p>Lorem ipsum</p>
            <p>UserName is: {props.username}</p>
        </div>
    );
}

export default userOutput;
